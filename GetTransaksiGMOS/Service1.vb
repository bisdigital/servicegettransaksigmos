﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Threading
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class Service1
    Private Shared AsyncOpDone As New System.Threading.AutoResetEvent(False)
    'Dim sqlcon As SqlConnection = New SqlConnection("host=;username=;pwd=;")
    Public dt As DataTable = New DataTable()
    Public ds As DataSet = New DataSet()
    Dim m_thread As Thread = Nothing

    Public Sub onDebug()
        OnStart(Nothing)
    End Sub


    Protected Overrides Sub OnStart(ByVal args() As String)
        m_thread = New Thread(New ThreadStart(AddressOf mainproc))
        m_thread.Start()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

    Private Sub mainproc()
        Dim w As WaitCallback = New WaitCallback(AddressOf StartService)
        Try
            ThreadPool.QueueUserWorkItem(w)
            AsyncOpDone.WaitOne()
            mainproc()
        Catch generatedExceptionName As ThreadAbortException
            Thread.ResetAbort()
        Catch ex As Exception
            AsyncOpDone.Set()
        End Try
    End Sub

    Private Sub StartService()
        Try
            GetTransaksi()
            UpdateStatus()
        Catch ex As Exception

        End Try

        'Lama nunggu service berjalan kembali
        Thread.Sleep(60000)
        AsyncOpDone.Set()
    End Sub

    Function GetTransaksi()
        'query select

        Dim url As String = "http://18.141.184.60/data/API/transaction"
        Dim method As String = "POST"

        Dim param As RequestData = New RequestData()
        param.kode_seller = "GCM01"
        param.tipe_data = "all" 'ALL(semua transaksi)/ selected(yang belum diambil)

        Dim SendAPI As String = SendingAPI(url, JsonConvert.SerializeObject(param), method)

        Dim ob = JObject.Parse(SendAPI)
        Dim responseAPI As ResponseData = ob.ToObject(Of ResponseData)()
        If responseAPI.status = "success" Then
            Dim ListValue As List(Of DataTransaksi) = responseAPI.values

            'check jumlah list transaksi
            If ListValue.Count > 0 Then

                'looping per transaksinya
                For i = 0 To ListValue.Count - 1
                    Dim DetailTransaksi As DataTransaksi = ListValue(i)
                    Dim IDtransaksi = DetailTransaksi.id_transaction
                    Dim status = DetailTransaksi.status
                    Dim alamat = DetailTransaksi.alamat_shipto
                    Dim payment = DetailTransaksi.payment_name

                    'insert DATABSE GCM
                Next
            End If

        End If

    End Function

    Function UpdateStatus()
        'Select Data di DATABASE GCM (QUERY Cari status 3 hari kebelakang)

        'HIT API Update
    End Function

    Public Function SendingAPI(ByVal Url As String, ByVal Body As String, ByVal Method As String) As String
        Dim Response As String = ""
        Dim Array As Byte()
        Dim Request As WebRequest
        Dim Stream As Stream
        Dim WebResponse As WebResponse
        Dim Reader As StreamReader
        Try
            Request = WebRequest.Create(Url)
            Array = Encoding.UTF8.GetBytes(Body)
            Request.Method = Method
            Request.ContentType = "application/json"
            Request.ContentLength = Array.Length
            Stream = Request.GetRequestStream()
            Stream.Write(Array, 0, Array.Length)
            Stream.Close()
            WebResponse = Request.GetResponse()
            Stream = WebResponse.GetResponseStream()
            Reader = New StreamReader(Stream)
            Response = Reader.ReadToEnd()
            Reader.Close()
        Catch ex As Exception
            Dim e = ex.Message.ToString()
        End Try
        Return Response
    End Function

End Class
