﻿Public Class MasterClass

End Class

Public Class RequestData
    Public kode_seller As String
    Public tipe_data As String
End Class

Public Class ResponseData
    Public status As String = ""
    Public values As List(Of DataTransaksi)
End Class

Public Class DataTransaksi
    Public id_transaction As String
    Public status As String
    Public kode_sales As String
    Public kode_buyer As String
    Public kode_seller As String
    Public tgl_transaksi As String
    Public tgl_permintaan_kirim As String
    Public alamat_shipto As String
    Public kode_shipto As String
    Public kode_billto As String
    Public alamat_billto As String
    Public payment_name As String
    Public ongkos_kirim As String
    Public total_transaksi As String
    Public data_barang As List(Of ListBarang)
End Class

Public Class ListBarang
    Public kode As String
    Public nama As String
    Public uom As String
    Public qty_order As String
    Public harga_asli_satuan As String
End Class